const assert = require('chai').assert;
const fetch = require('node-fetch');
const { Users } = require('../../delilah_resto/src/database/sync')
//Limpie la base de datos despues de cada it()
afterEach(async () => {
    await Users.destroy({
        where: {
            username: 'gpuffin'
        }
    })
})
describe('##### Testo de REGISTRO DE USUARIOS #####', () => {

    it('Devuelve status(401), si no se completo un campo en el registro.', async () => {
        const data = []
        await fetch('http://localhost:3000/users/sign-up',
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            })
            .then(response =>
                assert.strictEqual(response.status, 401))

    });

    it('Devuelve status(402), si el usuario o mail ya están registrados', async () => {
        const data = {
            "username": "quirogatdf",
            "fullName": "Grepper Puffin",
            "email": "quirogatdf@delilah.com",
            "phone": "2964111222",
            "address": "1 Logan PIKensington",
            "password": "123"
        };
        await fetch('http://localhost:3000/users/sign-up',
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            })
            .then(response =>
                assert.strictEqual(response.status, 402))

    });

    it('Devuelve status(201), si el usuario se registra exitosamente.', async () => {
        const data = {
            "username": "gpuffin",
            "fullName": "Grepper Puffin",
            "email": "nexus@delilah.com",
            "phone": "2964111222",
            "address": "1 Logan PIKensington",
            "password": "123"
        }

        await fetch('http://localhost:3000/users/sign-up',
            {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            })
            .then(response =>
                assert.strictEqual(response.status, 201))
    })

});