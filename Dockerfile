FROM node:17-alpine3.12

COPY . /delilah-be
RUN cd /delilah-be && npm install && npm audit fix
WORKDIR /delilah-be
CMD node index.js

