# Mi App Live: Delilah Resto
Sprint 04: "Mi APP Full", del curso de DWBE de Acamica

### 1. Coneccion a la instancia EC2
A través de un cliente SSH, podrá conectarse a la instancia con el archivo .pem válido y siguiendo las indicaciones de AWS Console

### 2. Verificaciones iniciales
**NGINX**
Conectarse a la instancia EC2 con el archivo .pem correspondiente a través de un cliente SSH.
Una vez logueado verificar que el servicio NGINX este iniciado con el siguiente comando:

`sudo systemctl status nginx.service`

**PM2**
Verificar que la API se encuentre online con el siguiente comando:

`pm2 status`

En caso de que la API no se encuentre en ejecución, deberá ejecutar la siguiente instruccion dentro del directorio /delilah:

`pm2 start ecosystem.config.js --env local`

### 3. Docker run
1. Clonar el repositorio
2. Crear y configurar dentro del archivo .env, las claves necesarias para el funcionamiento de la integracion.
4. Crear la imagen de Docker

`docker build -t quirogatdf/delilah-be .`

5. Una vez generada la imagen correr el Dockerfile

`docker run -it -d -p 5000:5000 --name delilah-be-container quirogatdf/delilah-bep`


### 4. Consultas
**Postman**
Puede realizar las consultar a la API, a través de Postman
Link consultas en Postman:
[Postman](https://www.getpostman.com/collectionsfee6a10b)

Swagger
Puede realizar las consultas a la API, a través de Swagger, ingresando al [https://www.delilah-resto/api-docs](https://www.delilah-resto.tk/api-docs)

Usuario disponibles para consultas:
- Administrador:
- - Usuario: Administrador
- - Password: pass
- Usuario habilitado:
- - Usuario: andif
- - Password: dym
- Usuario deshabilitado:
- - Usuario: queen_freddie
- - Password: asd

### 5. Integraciones
**API de Google**

Ingresar al link de la demo [https://www.delilah-resto.tk](https://www.delilah-resto.tk), hacer clic en el boton "Google". Si se logueo correctamente, será redireccionado a la prueba de Pasarela de pago.

**Api de LinkedIn**

Ingresar al link de la demo [https://www.delilah-resto.tk](https://www.delilah-resto.tk), hacer clic en el botón "Linkedin". Si se logueo correctamente, se mostrará en el navegador un archivo.json con los datos obtenidos. 

**Api de Auth0**

Ingresar al link de la demo [https://www.delilah-resto.tk](https://www.delilah-resto.tk), hacer clic en el boton "Auth0", y será redireccionado a la interfaz de login de Auth0.

**API de Mercadopago**

Ingresar al link de la demo [https://www.delilah-resto.tk](https://www.delilah-resto.tk), hacer click en el boton "simulate payment with Mercadopago". 
Tarjetas de Pruebas: Podes testear la integración con las tarjetas que provee Mercado Pago en su documentación.

| Tarjeta          | Numero              | Código CVC | Fecha vto. |
| ---------------- | ------------------- | ---------- | ---------- |
| Mastercard       | 5031 7557 3453 0604 | 123        | 11/25      |
| Visa             | 4509 9535 6623 3704 | 123        | 11/25      |
| American Express | 3711 803032 57522   | 1234       | 11/25      |


*Página Web*
Link de la demo:
[https://www.delilah-resto.tk](https://www.delilah-resto.tk)
Solamente se puede realizar prueba de login

