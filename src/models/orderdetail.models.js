module.exports = (sequelize, type) => {
    return sequelize.define('Order_details', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        quantity: type.INTEGER,
        subtotal: type.FLOAT,
    },
        {
            sequelize,
            timestamps: false,
        }
    )
}