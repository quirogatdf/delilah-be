module.exports = (sequelize, type) => {
    return sequelize.define('Users', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        username: type.STRING,
        fullName: type.STRING,
        email: type.STRING,
        phone: type.STRING(20),
        address: type.STRING,
        password: type.STRING,
        token: type.STRING(1000),
        is_admin: {
            type: type.BOOLEAN,
            defaultValue: 0
        },
        is_active: {
            type: type.BOOLEAN,
            defaultValue: 1
        },
    },
        {
            sequelize,
            timestamps: false,
        }
    )
}