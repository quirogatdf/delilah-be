module.exports = (sequelize, type) => {
    return sequelize.define('Addresses', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        address: type.STRING(50),
    },
        {
            sequelize,
            timestamps: false,
        },
    );
}