const { Users } = require("../database/sync");
const { jwt } = require('../config');
const { sign } = require('jsonwebtoken')


class LoginController {
    static async hello (req,res){
        const username = req.body.username;
        const password = req.body.password;
        console.log(username)
        res.send({message: username, password: password});
    }
    /* Control de inicio de sesión */
    static async login(req, res) {
        const username = req.body.username
        const password = req.body.password
        //const { username, password } = req.body
        const findUser = await Users.findOne({
            where: {
                username: username,
                //password: password
            }, raw: true
        })
        //console.log(findUser);

        if (findUser) {
            if (findUser.password === password) {
                if (findUser.is_active) {
                    /* Generar Token */
                    const newToken = sign(findUser, jwt.key, {
                        expiresIn: jwt.expires_time
                    })
                    /* Guardar Token en la BD */
                    await Users.update({
                        token: newToken
                    }, {
                        where: { id: findUser.id }
                    })
                    console.log('Bienvenido');
                    res.send({ auth: true, message: `Bienvenido, ${findUser.fullName}`, token: newToken });
                } else {
                    res.send({ auth: false, message:`El usuario ${findUser.username} se encuentra inhabilitado, consulte al administrador`})
                }
            } else {
                res.send({ auth: false, message: 'Contraseña inválida'})
            }

        } else {
            res.status(404).json({ auth: false, message: 'Usuario Inválido'})

        }
    }

}

module.exports = LoginController
