const { Addresses } = require('../database/sync');
const { jwt } = require('../config');

class AddressesControllers {
    /*Listar todas las direcciones del usuario*/
    static async getAll(req, res) {
        const addresses = await Addresses.findAll({
            where: {
                userId: req.user.id
            }
        })
        res.status(200).send({
            title: 'Listado de direcciones',
            addresses: addresses
        })
    }
    /*Listar la direccion seleccionada, del usuario */
    static async getById(req, res) {
        const id = req.params.id
        const address = await Addresses.findOne({
            where: {
                userId: req.user.id,
                id: id
            }
        })
        if (address) {
            res.status(201).send(address);
        } else {
            res.status(500).send({ message: 'No se encontro la dirección seleccionada' })
        }
    }
    /* Agregar una nueva dirección */
    static async add(req, res) {
        const userId = req.user.id
        const { address } = req.body
        try {
            await Addresses.create({ address: address, userId: userId })
            res.status(201).json({ message: `se ha cargado con éxito la nueva dirección: ${address}` })
        } catch {
            res.status(500).json({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
        }
    }
    static async updateRegistry(req, res) {
        const id = req.params.id;
        const userId = req.user.id;
        const Address = await Addresses.findOne({
            where: {

                userId: userId,
                id: id
            }
        })
        console.log(Address)
        if (Address) {
            await Addresses.update(req.body, {
                where: {
                    id: id
                }
            })
            res.status(201).json({ message: `Se actualizó la dirección con éxito!` })
        } else {
            res.status(404).json({ error: 'Not found Id.' })
        }
    }
    static async delete(req, res) {
        const id = req.params.id
        const userId = req.user.id
        await Addresses.destroy({
            where: {
                id: id,
                userId: userId
            }
        })
            .then((deletedRecord) => {
                if (deletedRecord === 1) {
                    res.status(201).send({ message: 'Dirección eliminado satisfactoriamente.' })
                } else {
                    res.status(404).send({ error: 'Not found Id.' })
                }
            })
            .catch((error) => {
                res.status(500).json({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
            })
    }
}

module.exports = AddressesControllers