const { Users } = require('../database/sync')
class UsersController {
  /* Listar todos los usuarios */
  static async getAll(req, res) {
    const usuarios = await Users.findAll();
    res.status(201).json({
      title: 'Listado de usuarios',
      users_data: usuarios,
    });
  };

  /* Listar usuario seleccionado */
  static async getById(req, res) {
    const id = parseInt(req.params.id);
    const usuario = await Users.findByPk(id)
    if (usuario.length !== 0) {
      res.status(201).send(usuario)
    } else {
      res.status(500).json({ message: 'No se encontró el usuario seleccionado' })
    }
  }
  /* Registrar un nuevo usuario */
  static async add(req, res) {
    const { username, fullname, email, phone, address, password } = req.body;
    try {
      let user = await Users.create(req.body)
      res.status(201).json({ message: `El usuario ${username}, se ha creado con éxito` })
    } catch (error) {
      res.status(500).json({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
    }
  }
  /* El administrador puede suspender a un usuario */
  static async suspend(req, res) {
    const id = parseInt(req.params.id);
    try {
      const usuario = await Users.findByPk(id);
      if (usuario) {
        //Verifica que el usuario este activo
        if (usuario.is_active) {
          await Users.update({ is_active: false }, {
            where: {
              id: id
            }
          })
          res.status(201).send(`El usuario ${usuario.username} se encuentra suspendido.`);
        } else {
          await Users.update({ is_active: true }, {
            where: {
              id: id
            }
          });
          res.status(201).send(`El usuario ${usuario.username} se encuentra habilitado nuevamente.`);
        }

      } else {
        res.status(404).json({ error: 'Not found Id.' });
      }

    } catch (error) {
      console.error(error);
    }
  }
  /* Actualizar los datos del usuarios seleccionado */
  static async updateRegistry(req, res) {
    const id = parseInt(req.params.id);
    // const { username = false, fullName = false, telefono = false, direccion = false } = req.body;
    try {
      const usuario = await Users.findByPk(id);
      if (usuario) {
        await Users.update(req.body, {
          where: { id: id }
        });
        res.status(201).send('Usuario actualizado correctamente');
      } else {
        res.status(404).json({ error: 'Not found Id.' })
      }
    } catch (error) {
      res.status(500).json({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
    }
  }

  /* Eliminar el usuario seleccionado */
  static async delete(req, res) {
    const id = parseInt(req.params.id);
    await Users.destroy({
      where: {
        id: id
      }
    })
      .then((deletedRecord) => {
        if (deletedRecord === 1) {
          res.status(201).json({ message: 'Usuario eliminado satisfactoriamente.' })
        } else {
          res.status(404).json({ error: 'Not found Id.' })
        }
      })
      .catch((error) => {
        res.status(500).json({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
      })
  }

}

module.exports = UsersController;