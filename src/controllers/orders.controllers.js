const { verify } = require('jsonwebtoken');
const { jwt } = require('../config/');
const { Orders, OrderDetails, Products, Addresses, PaymentMethods } = require('../database/sync');


class OrdersControllers {
    /* Listar todos los pedidos*/
    static async getAll(req, res) {

        if (req.user.is_admin) {
            // El administrador puede ver todos los pedidos
            const orders = await Orders.findAll();
            res.status(200).send({
                title: 'Pedidos',
                Orders_data: orders
            })
        } else {
            // Los clientes solo puede ver sus pedidos
            const orders = await Orders.findAll({
                where: {
                    userId: req.user.id
                }
            });
            res.status(200).send({
                Title: 'Historial de pedidos',
                Orders_data: orders
            });
        }

    }
    /* Listar el pedido seleccionado */
    static async getById(req, res) {
        const id = parseInt(req.params.id);
        let result = await Orders.findByPk(id)
        //El administrador puede ver el detalle de cualquier pedido, y los usuarios solo pueden ver sus pedidos
        if (result) {
            if (req.user.is_admin) {
                res.status(201).send(result)
            } else {
                if (result.userId === req.user.id) {
                    res.status(201).send(result)
                } else {
                    res.status(500).send({ message: 'Usted no tiene permiso, para acceder al pedido solicitado' })
                }
            }
        } else {
            res.status(500).send({ message: 'No se encontró pedido seleccionado' })
        }
    }

    /* Agregar un nuevo pedido*/
    static async add(req, res) {
        // Verifica si el cliente tiene un pedido pendiente
        const pendiente = await Orders.findOne(
            {
                where: {
                    userId: req.user.id,
                    state: 'Pendiente',
                }
            }
        );

        // Genera el pedido   
        if (!pendiente) {
            try {
                const { products } = req.body
                let monto = null
                let desc = ''
                // Calcular monto total
                for (let x in products) {
                    let product = await Products.findByPk(products[x].productId);
                    console.log(product);
                    let subtotal = product.precio * products[x].quantity;
                    desc = desc + (products[x].quantity).toString() + 'x' + product.description + ' '
                    console.log(desc)
                    monto = monto + subtotal;

                }
                //Generar order
                const address = await Addresses.findOne({//Verifica addressId, si no existe ingresa NULL
                    where: {
                        userId: req.user.id,
                        id: req.body.addressId
                    }
                })
                if (!address) {
                    req.body.addressId = null
                }
                //Verifica que el método de pago exista
                const payment_method = await PaymentMethods.findOne({ where: { id: req.body.payment_methodsId } });
                if (!payment_method) {
                    req.body.payment_methodsId = null
                }
                const data = { ...req.body, userId: req.user.id, total: monto, description: desc }
                const order = await Orders.create(data)
                //Cargar los productos en el detalle del pedido
                for (let x in products) {
                    let product = await Products.findByPk(products[x].productId);
                    let subtotal = product.precio * products[x].quantity;
                    await OrderDetails.create({ quantity: products[x].quantity, subtotal: subtotal, productsId: product.id, ordersId: order.id })
                }
                res.status(201).json({ message: `El pedido se ha generado con éxito` });
            } catch (error) {
                console.error(error);
            }
        } else {
            res.status(500).json({ message: 'Usted tiene un pedido pendiente' })
        }
    }
    /*El usuario puede confirmar su pedido */
    static async confirm(req, res) {
        const id = parseInt(req.params.id)
        const userId = req.user.id
        try {
            const pendiente = await Orders.findOne({
                where: {
                    id: id,
                    userId: userId,
                    state: 'Pendiente'
                }
            });
            if (pendiente) {
                let monto = null
                let desc = ''
                let products = await OrderDetails.findAll({
                    where: {
                        ordersId: id
                    }
                })
                // Calcular monto total
                for (let x in products) {
                    let product = await Products.findByPk(products[x].productsId);
                    console.log(product);
                    let subtotal = product.precio * products[x].quantity;
                    desc = desc + (products[x].quantity).toString() + 'x' + product.description + ' '
                    console.log(desc)
                    monto = monto + subtotal;

                }
                await Orders.update({
                    state: 'Confirmado',
                    description: desc,
                    total: monto
                }, {
                    where: {
                        id: id
                    }
                });
                res.status(200).send({ title: 'Recibimos tu pedido', message: `${req.user.fullName}, gracias por pedir a Delilah. Puedes seguir tu pedido con el siguiente código #${req.params.id}` })
            } else {
                res.status(500).send('No se encontro ningún pedido pendiente');
            }
        } catch (error) {
            console.error(error);
        }
    }
    /* El administrador puede actualizar el estado del pedido*/
    static async updateRegistry(req, res) {
        const id = parseInt(req.params.id);
        const { state = false } = req.body
        try {
            const order = await Orders.findByPk(id)
            if (order) {
                if (state) {
                    await Orders.update({ state: state }, {
                        where: { id: id }
                    });
                } else {
                    res.status(201).send({ message: `No se actualizo el pedido` })
                }
                res.status(201).json({ message: `Se actualizó el estado del pedido.` })
            } else {
                res.status(404).send({ error: 'Not found Id.' })
            }
        } catch (error) {
            res.status(500).send({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
        }
    }

    /* Eliminar el Pedido seleccionado */
    static async delete(req, res) {
        const id = parseInt(req.params.id);
        //Eliminar productos del pedido
        await OrderDetails.destroy({
            where: {
                OrdersId: id
            }
        })
        //Eliminar pedido
        await Orders.destroy({
            where: {
                id: id
            }
        })
            .then((deletedRecord) => {
                if (deletedRecord === 1) {
                    res.status(201).send({ message: 'Pedido eliminado satisfactoriamente.' })
                } else {
                    res.status(404).send({ error: 'Not found Id.' })
                }
            })
            .catch((error) => {
                res.status(500).send({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
            })
    }
}

module.exports = OrdersControllers;