const { PaymentMethods } = require('../database/sync');

class PaymentMethodsControllers {
    /* Listar todos los métodos de pago */
    static async getAll(req, res) {
        const payment_methods = await PaymentMethods.findAll();
        res.json({
            mensaje: 'Formas de Pago',
            PaymentMethod_data: payment_methods
        })
    }
    /* Listar el método de pago seleccionado */
    static async getById(req, res) {
        const id = parseInt(req.params.id);
        let result = await PaymentMethods.findByPk(id)
        if (result) {
            res.status(201).send(result)
        } else {
            res.status(500).json({ message: 'No se encontró el método de pago seleccionado' })
        }
    }

    /* Agregar un método de pago nuevo */
    static async add(req, res) {
        const {name} = req.body;
        try {
            await PaymentMethods.create(req.body)
            res.status(201).json({ message: `se ha creado con éxito el método de pago: ${name}` })
        } catch (error) {
            res.status(500).json({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
        }
    }

    /* Actualizar el registro del método de pago seleccionado */
    static async updateRegistry(req, res) {
        const id = parseInt(req.params.id);
        try {
            const payment_method = await PaymentMethods.findByPk(id)
            if (payment_method) {
                await PaymentMethods.update(req.body, {
                    where: { id: id }
                });
                res.status(201).json({ message: `Método de pago actualizado` })
            } else {
                res.status(404).json({ error: 'Not found Id.' })
            }
        } catch (error) {
            res.status(500).json({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
        }
    }

    /* Eliminar el método de pago seleccionado */
    static async delete(req, res) {
        const id = parseInt(req.params.id);
        await PaymentMethods.destroy({
            where: {
                id: id
            }
        })
            .then((deletedRecord) => {
                if (deletedRecord === 1) {
                    res.status(201).json({ message: 'Método de pago eliminado satisfactoriamente.' })
                } else {
                    res.status(404).json({ error: 'Not found Id.' })
                }
            })
            .catch((error) => {
                res.status(500).json({ error: 'Algo salió mal. Vuelva a intentarlo o póngase en contacto con un administrador.', message: error })
            })
    }
}

module.exports = PaymentMethodsControllers;