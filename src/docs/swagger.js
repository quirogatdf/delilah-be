//##############################
//###### SWAGGER USUARIOS ######
//##############################
/**
 * @swagger
 * paths:
 *   /login:
 *     post:
 *       tags:
 *         - Login and register
 *       description: Iniciar sesion
 *       parameters:
 *         - name: username
 *           description: nombre de usuario
 *           in: formData
 *           required: false
 *           type: string
 *         - name: password
 *           description: contraseña del usuario
 *           in: formData
 *           required: false
 *           type: string
 *       responses:
 *         "201":
 *           description : success
 *         "404":
 *           description : Invalid username/password supplied
 *
 * /users/sign-up:
 *   post:
 *     tags:
 *       - Login and register
 *     description: Registrar un nuevo usuario, no permite mail duplicados
 *     parameters:
 *       - name: username
 *         description: Usuario
 *         in: formData
 *         required: false
 *         type: string
 *         schema:
 *           example: andif
 *       - name: fullName
 *         description: Nombre completo
 *         in: formData
 *         required: false
 *         type: string
 *         schema:
 *           example: Frias Andrea Florencia
 *       - name: email
 *         description: Email
 *         in: formData
 *         required: false
 *         type: string
 *         schema:
 *           example: andif@delilah.com
 *       - name: phone
 *         description: telefono
 *         in: formData
 *         required: false
 *         type: integer
 *         schema:
 *           example: 2964425164
 *       - name: address
 *         description: Direccion
 *         in: formData
 *         required: false
 *         type: string
 *         schema:
 *           example: 438 Dark Spurt
 *       - name: password
 *         description: Contraseña
 *         in: formData
 *         required: false
 *         type: string
 *         schema:
 *           example: coffe21
 *     responses:
 *       "201":
 *         description : Success
 *       "401":
 *         description: Campos sin completar
 *       "402":
 *         description : Mail y/o username duplicado
 * /api/users:
 *  get:
 *    tags:
 *      - Usuarios
 *    description: Mostrar listado de usuarios
 *    summary: Consultar listado de usuarios
 *    parameters:
 *      - name: Authorization
 *        in: header
 *        description: ingresar Token proporcionado en el login.
 *        required: true
 *        schema:
 *          type: string
 *          example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *    responses:
 *      "201":
 *        description: success
 *
 * /users/{id}:
 *   get:
 *     tags:
 *       - Usuarios
 *     description: El administrador puede ver la información del usuario seleccionado
 *     summary: See user by Id
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 1
 *     responses:
 *       "201":
 *          description: success
 *
 * /users/edit/{id}:
 *   put:
 *     tags:
 *       - Usuarios
 *     description: El administrador puede editar los datos del usuario
 *     summary: Update user information
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *           example: 2
 *       - name: username
 *         description: Usuario
 *         in: formData
 *         required: false
 *         type: string
 *         schema:
 *           example: mateo020
 *       - name: fullName
 *         description: Nombre completo
 *         in: formData
 *         required: false
 *         type: string
 *         schema:
 *           example: Quiroga Mateo Benjamin
 *       - name: email
 *         description: Email
 *         in: formData
 *         required: false
 *         type: integer
 *         schema:
 *           example: name@domain.com
 *       - name: phone
 *         description: Telefono
 *         in: formData
 *         required: false
 *         type: integer
 *         schema:
 *           example: 2964425164
 *       - name: address
 *         description: Direccion
 *         in: formData
 *         required: false
 *         type: string
 *         schema:
 *           example: 438 Dark Spurt
 *       - name: password
 *         description: Contraseña
 *         in: formData
 *         required: false
 *         type: string
 *         schema:
 *           example: coffe21
 *     responses:
 *       "201":
 *         description: Success
 *
 * /users/suspend/{id}:
 *   put:
 *     tags:
 *       - Usuarios
 *     description: El administrador puede habilitar o suspender al usuario seleccionado.
 *     summary: Suspend user
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 2
 *     responses:
 *       "201":
 *          description: Success
 *
 * /users/delete/{id}:
 *   delete:
 *     tags:
 *       - Usuarios
 *     description: El administrador puede eliminar el usuario seleccionado.
 *     summary: Delete user
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 2
 *     responses:
 *       "201":
 *          description: Success
*/
//###############################
//###### SWAGGER PRODUCTOS ######
//###############################
/**
 * @swagger
 * /products:
 *  get:
 *    tags:
 *      - Productos
 *    description: Los usuarios registrados pueden ver el listado de productos
 *    summary: Get information from  all products
 *    parameters:
 *      - name: Authorization
 *        in: header
 *        description: ingresar Token proporcionado en el login.
 *        required: true
 *        schema:
 *          type: string
 *          example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *    responses:
 *      "200":
 *        description: success
 *
 * /products/{id}:
 *   get:
 *     tags:
 *       - Productos
 *     description: Los usuarios pueden ver el detalle del producto seleccionado
 *     summary: See product by Id
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 1
 *     responses:
 *       "201":
 *          description: success
 * /products/add:
 *   post:
 *     tags:
 *       - Productos
 *     description: El administrador puede agregar productos nuevos
 *     summary: Create a new Product
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token de un usuario Administrador proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: nombre
 *         description: Nombre del producto
 *         in: formData
 *         required: true
 *         type: string
 *       - name: description
 *         description: Descripcion corta del producto
 *         in: formData
 *         required: true
 *         type: string
 *       - name: precio
 *         description: Precio
 *         in: formData
 *         required: true
 *         type: integer
 *     responses:
 *       "201":
 *         description: Success
 * /products/edit/{id}:
 *   put:
 *     tags:
 *       - Productos
 *     description: El administrador puede actualizar la informacion del producto
 *     summary: Update product information
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token de un usuario Administrador proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 2
 *       - name: nombre
 *         description: Nombre del producto
 *         in: formData
 *         required: false
 *         type: string
 *         schema:
 *           example: Sushi
 *       - name: description
 *         description: Una descripción corta del producto.
 *         in: formData
 *         required: false
 *         type: string
 *       - name: precio
 *         description: precio
 *         in: formData
 *         required: false
 *         type: integer
 *     responses:
 *       "201":
 *         description: Success
 * /products/delete/{id}:
 *   delete:
 *     tags:
 *       - Productos
 *     description: El administrador puede eliminar un producto.
 *     summary: Delete product
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token de un usuario Administrador proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 2
 *     responses:
 *       "201":
 *         description: Success
 */

//###############################
//###### SWAGGER DIRECCIONES ######
//###############################
/**
 * @swagger
 * /address:
 *  get:
 *    tags:
 *      - Direcciones
 *    description: Los usuarios registrados pueden ver todas sus direcciones registradas
 *    summary: Get information from all address
 *    parameters:
 *      - name: Authorization
 *        in: header
 *        description: ingresar Token proporcionado en el login.
 *        required: true
 *        schema:
 *          type: string
 *          example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *    responses:
 *      "200":
 *        description: success
 *
 * /address/{id}:
 *   get:
 *     tags:
 *       - Direcciones
 *     description: Los usuarios pueden ver la dirección seleccionada.
 *     summary: See address by Id
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 1
 *     responses:
 *       "201":
 *          description: success
 * /address/add:
 *   post:
 *     tags:
 *       - Direcciones
 *     description: Un usuario registrado puede cargar una nueva dirección.
 *     summary: Create a new Address
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: address
 *         description: Dirección
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       "201":
 *         description: Success
 * /address/edit/{id}:
 *   put:
 *     tags:
 *       - Direcciones
 *     description: Un usuario registrado puede actualizar la dirección seleccionada.
 *     summary: Update address
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 2
 *       - name: address
 *         description: Direccion
 *         in: formData
 *         required: false
 *         type: string
 *         schema:
 *           example: Campora 1366
 *     responses:
 *       "201":
 *         description: Success
 * /address/delete/{id}:
 *   delete:
 *     tags:
 *       - Direcciones
 *     description: Un usuario registrado puede eliminar una dirección.
 *     summary: Delete Address
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 2
 *     responses:
 *       "201":
 *         description: Success
 */
//###############################
//###### SWAGGER METODOS DE PAGO ######
//###############################
/**
 * @swagger
 * /payment_methods:
 *  get:
 *    tags:
 *      - Métodos de pago
 *    description: Los usuarios registrados pueden ver todos los métodos de pago.
 *    summary: Get information from all Payment methods
 *    parameters:
 *      - name: Authorization
 *        in: header
 *        description: ingresar Token proporcionado en el login.
 *        required: true
 *        schema:
 *          type: string
 *          example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *    responses:
 *      "200":
 *        description: success
 *
 * /payment_methods/{id}:
 *   get:
 *     tags:
 *       - Métodos de pago
 *     description: Los usuarios pueden ver el método de pago seleccionado.
 *     summary: See Payment method by Id
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 1
 *     responses:
 *       "201":
 *          description: success
 * /payment_methods/add:
 *   post:
 *     tags:
 *       - Métodos de pago
 *     description: El Administrador puede cargar un nuevo método de pago.
 *     summary: Create a new Payment method
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: name
 *         description: Método de pago
 *         in: formData
 *         required: true
 *         type: string
 *     responses:
 *       "201":
 *         description: Success
 * /payment_methods/edit/{id}:
 *   put:
 *     tags:
 *       - Métodos de pago
 *     description: El Administrador puede actualizar el método de pago seleccionado.
 *     summary: Update Payment method
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 2
 *       - name: name
 *         description: Método de pago
 *         in: formData
 *         required: false
 *         type: string
 *         schema:
 *           example: Bitcoin
 *     responses:
 *       "201":
 *         description: Success
 * /payment_methods/delete/{id}:
 *   delete:
 *     tags:
 *       - Métodos de pago
 *     description: El administrador eliminar el método de pago seleccionado.
 *     summary: Delete Payment method
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 2
 *     responses:
 *       "201":
 *         description: Success
 */

//***********************
//*** SWAGGER PEDIDOS ***
//***********************

/**
 * @swagger
 * /orders:
 *  get:
 *    tags:
 *      - Pedidos
 *    description: El usuario puede ver el historial de sus pedidos y el Administrador puede ver todos los pedidos.
 *    summary: Get information from all Orders
 *    parameters:
 *      - name: Authorization
 *        in: header
 *        description: ingresar Token proporcionado en el login.
 *        required: true
 *        schema:
 *          type: string
 *          example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *    responses:
 *      "200":
 *        description: success
 *
 * /orders/{id}:
 *  get:
 *    tags:
 *      - Pedidos
 *    description: El usuario solamente puede ver su pedido y el administrador puede ver cualquier pedido.
 *    summary: Get Order by Id
 *    parameters:
 *      - name: Authorization
 *        in: header
 *        description: ingresar Token proporcionado en el login.
 *        required: true
 *        schema:
 *          type: string
 *          example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *      - name: id
 *        in: path
 *        required: true
 *        schema:
 *          type: number
 *          example: 1   
 *    responses:
 *      "201":
 *        description: success
 *      "500":
 *        description: No se encontró el pedido seleccionado
 *
 * /orders/add:
 *  post:
 *    tags:
 *      - Pedidos
 *    description: Los usuarios registrados pueden realizar un nuevo pedido, en caso de que no tengan ninguno pendiente
 *    summary: Add order
 *    consumes:
 *      - application/json
 *    produces:
 *      - application/json
 *    parameters:
 *      - name: Authorization
 *        in: header
 *        description: Ingresar Token proporcionado en el login.
 *        required: true
 *        schema:
 *          type: string
 *          example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *      - in: body
 *        name: Pedido
 *        description: Objeto pedido
 *        schema:
 *          type: object
 *          properties:
 *            payment_methodsId:
 *              type: integer
 *              example: 2
 *            addressId:
 *              type: integer
 *              example: 1
 *            products:
 *              type: array
 *              items:
 *                type: object
 *                properties:
 *                  productId:
 *                    type: integer
 *                    example: 2
 *                  quantity:
 *                    type: integer
 *                    example: 1
 *    responses:
 *      201:
 *        description: Sucess
 *      500:
 *        description: El usuario ya tiene un pedido pendiente.
 * 
 * /orders/confirm/{id}:
 *   put:
 *     tags:
 *       - Pedidos
 *     description: Los usuarios pueden ver el método de pago seleccionado.
 *     summary: Confirm Order
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 1
 *     responses:
 *       "201":
 *          description: success
 * /orders/edit/{id}:
 *   put:
 *     tags:
 *       - Pedidos
 *     description: El Administrador puede actualizar el estado del pedido.
 *     summary: Update Order status
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 1
 *       - name: state
 *         in: formData
 *         required: false
 *         type: string
 *         schema:
 *           example: "En preparacion"
 *     responses:
 *       "201":
 *          description: success
 * /orders/delete/{id}:
 *   delete:
 *     tags:
 *       - Pedidos
 *     description: El administrador puede eliminar el pedido seleccionado.
 *     summary: Delete Order
 *     parameters:
 *       - name: Authorization
 *         in: header
 *         description: Ingresar Token proporcionado en el login.
 *         required: true
 *         schema:
 *           type: string
 *           example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *       - name: id
 *         in: path
 *         required: true
 *         schema:
 *           type: number
 *           example: 2
 *     responses:
 *       "201":
 *         description: Success
 *       "404":
 *         description: No se encontró Id del pedido
 */
//***********************
//*** SWAGGER PEDIDOS ***
//***********************

/**
 * @swagger
 * /orders/{orderid}/order_details:
 *  get:
 *    tags:
 *      - Detalle del Pedido
 *    description: El usuario puede ver el detalle de su pedido antes de confirmarlo.
 *    summary: Get Order detail
 *    parameters:
 *      - name: Authorization
 *        in: header
 *        description: ingresar Token proporcionado en el login.
 *        required: true
 *        schema:
 *          type: string
 *          example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *      - name: orderid
 *        in: path
 *        description: Ingresar el Id del pedido
 *        required: true
 *        schema:
 *          type: number
 *          example: 2
 *    responses:
 *      "201":
 *        description: success
 * /orders/{orderid}/order_details/add:
 *  post:
 *    tags:
 *      - Detalle del Pedido
 *    description: El usuario puede cargar un nuevo producto o modificar la cantidad de uno existente..
 *    summary: Add product to order
 *    parameters:
 *      - name: Authorization
 *        in: header
 *        description: ingresar Token proporcionado en el login.
 *        required: true
 *        schema:
 *          type: string
 *          example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *      - name: orderid
 *        in: path
 *        description: Ingresar el Id del pedido
 *        required: true
 *        schema:
 *          type: number
 *          example: 2
 *      - name: productsId
 *        in: formData
 *        description: Ingresar el Id del producto
 *        required: true
 *        schema:
 *          type: number
 *          example: 2
 *      - name: quantity
 *        in: formData
 *        description: Cantidad del producto
 *        required: true
 *        schema:
 *          type: number
 *          example: 1
 *    responses:
 *      "201":
 *        description: success
 * /orders/{orderid}/order_details/delete/{id}:
 *  delete:
 *    tags:
 *      - Detalle del Pedido
 *    description: El usuario puede eliminar un producto del pedido antes de confirmarlo.
 *    summary: Delete product from Order
 *    parameters:
 *      - name: Authorization
 *        in: header
 *        description: ingresar Token proporcionado en el login.
 *        required: true
 *        schema:
 *          type: string
 *          example: "Bearer eyJhbGdf8iJIUzI1NiIsInR5cCI6IkpXVCJ1.eyJ1c2VyX2lkIjo0MiwiaWF0IjoxNTgzOTUyNDgpfQ.sKql5gYZr_Se5gGGDOFaK9c23DbY4OiTf74JauFNyHk"
 *      - name: orderid
 *        in: path
 *        description: Ingresar el Id del pedido
 *        required: true
 *        schema:
 *          type: number
 *          example: 2
 *      - name: id
 *        in: path
 *        description: Ingresar el Id del producto a eliminar del pedido.
 *        required: true
 *        schema:
 *          type: number
 *          example: 2
 *    responses:
 *      "201":
 *        description: success
 */
