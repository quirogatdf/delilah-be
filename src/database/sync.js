'use strict'
const {Sequelize} = require('sequelize');
const {sequelize} = require('./db');

const UsersModel = require('../models/users.models');
const ProductsModel = require('../models/products.models');
const PaymentMethodModel = require('../models/payment_methods.models')
const OrdersModel = require('../models/order.models')
const OrderDetailsModel=  require('../models/orderdetail.models')
const AddressesModels = require('../models/addresses.models')

const Users = UsersModel(sequelize, Sequelize);
const Products = ProductsModel(sequelize,Sequelize);
const PaymentMethods = PaymentMethodModel(sequelize, Sequelize);
const Orders = OrdersModel(sequelize, Sequelize);
const OrderDetails = OrderDetailsModel(sequelize, Sequelize);
const Addresses = AddressesModels(sequelize, Sequelize);

/* Claves foraneas*/
// Direcciones pertenece a Usuarios
Addresses.belongsTo(Users, {foreignKey: 'userId'});

// Pedido pertenece a Usuarios
Orders.belongsTo(Users, {foreignKey: 'userId'});
Orders.belongsTo(PaymentMethods, {foreignKey: 'payment_methodsId'});
Orders.belongsTo(Addresses, {foreignKey:'addressId'})

// Detalle del pedido a Pedido & Productos
OrderDetails.belongsTo(Products, {foreignKey:'productsId'});
OrderDetails.belongsTo(Orders, {foreignKey: 'ordersId'});


sequelize.sync({force: false}).then(()=>{
    console.log('Tablas sincronizadas.');
});
module.exports = {
    Users,
    Products,
    PaymentMethods,
    Orders,
    OrderDetails,
    Addresses
}