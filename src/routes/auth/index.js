const express = require('express');
const router = express.Router();
const google = require('./google');
const linkedin = require('./linkedin');
const auth0 = require('./auth0');


function prepareRoutes() {
  router.get('/failed', (req, res) => res.send('You Failed to log in!'));
  router.use(google());
  router.use(auth0());
  router.use(linkedin());
  return router

}

module.exports = prepareRoutes;
