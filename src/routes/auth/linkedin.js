const express = require('express');
const { linkedinAuthenticate, linkedinTokenAquisition } = require('../../middlewares/linkedAuth');

function prepareRouter() {
  const router = express.Router();

  router.get('/api/linkedin/auth', linkedinAuthenticate);

  router.get('/api/linkedin/auth/callback', linkedinTokenAquisition, (req, res) => {
    console.log(req.user);
    res.json(req.user);
  });


  return router;
}

module.exports = prepareRouter;
