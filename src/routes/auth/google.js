const express = require('express');
const passport = require('passport');

function prepareRouter() {
  const strategy_name = 'google';
  const router = express.Router();
  router.get('/api/google/auth', passport.authenticate(strategy_name, {
    scope: ['profile', 'email'],
    session: false,
  }));


  router.get('/api/google/auth/callback', passport.authenticate(strategy_name, {
      failureRedirect: '/failureLogin',
      session: false,
    }),
    function (req, res) {
      console.log('Todo salio bien');
      res.redirect('/mercadopago');
    });

  router.get('/failureLogin', (req, res) => {
    console.log('Algo Salio Mal');
    res.redirect('/');
  })

  return router;
}

module.exports = prepareRouter;
