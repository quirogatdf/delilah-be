const express = require('express');
const router = express.Router();
const {requiresAuth} = require('express-openid-connect')
function prepareRouter() {
  router.get ('/api/auth0/login', (req, res)=> {
    res.oidc.login({
      returnTo:'/api/auth0/user-info',
      authorizationParams: {
        scope: 'openid profile email admin:user'
      }});
  });
  router.get('/api/auth0/user-info', requiresAuth(), (req, res )=> {
  res.send(JSON.stringify(req.oidc.user));
  //res.send(req.oidc.isAuthenticated() ? 'Logged in' : 'Logged out');
  });
  router.get('/api/auth0/logout', (req,res) => {
    res.oidc.logout({ returnTo: '/' })
  });
  return router;
}
module.exports = prepareRouter;
