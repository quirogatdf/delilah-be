const { Router } = require ('express')
const ProductsController = require('../controllers/products.controller')
const {verifyToken, verifyRole} = require('../middlewares/auth.middleware')

const router = Router()


router
    .post('/add', verifyToken, verifyRole, ProductsController.add)                     /*Create*/
    .get('/', verifyToken, ProductsController.getAll)
    .get('/:id', verifyToken, ProductsController.getById)                              /*Read*/
    .put('/edit/:id', verifyToken, verifyRole, ProductsController.updateRegistry)      /*Update*/
    .delete('/delete/:id', verifyToken, verifyRole, ProductsController.delete)         /*Delete*/

module.exports = router