const express = require('express');
const mercadopago = require('mercadopago');

function initMercadoPagoRouter() {
  const router = express.Router();
  router.post('/api/process-payment', (req, res) => {
  mercadopago.configurations.setAccessToken(process.env.MP_ACCESS_TOKEN);
	const data = {
    ...req.body,
    transaction_amount: 8000
  };
    console.log(data);
  mercadopago.payment.save(data)
    .then(function(response) {
      const { status, status_detail, id } = response.body;
      console.log('Pago Exitoso', status_detail, id,);
      res.send({status, status_detail, id});
	})
	.catch(function(error) {
		res.send(error);
	})
  });
  return router;
}
module.exports = initMercadoPagoRouter
