const router = require('express').Router();

const productRoutes = require('./products.routes')
const userRoutes = require('./users.routes')
const loginRoutes = require('./login.routes')
const payment_methodsRoutes = require('./payment_methods.routes')
const ordersRoutes = require('./orders.routes')
const order_detailsRoutes = require('./order_details.routes')
const addressesRoutes = require('./addresses.routes')
router
    .use('/',loginRoutes)
    .use('/products',productRoutes)
    .use('/users', userRoutes)
    .use('/payment_methods', payment_methodsRoutes)
    .use('/orders', ordersRoutes)
    .use('/orders/:orderid/order_details', order_detailsRoutes)
    .use('/address', addressesRoutes)
module.exports = router
