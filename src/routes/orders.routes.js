const { Router } = require ('express')
const OrdersControllers= require('../controllers/orders.controllers')
const {verifyToken, verifyRole} = require('../middlewares/auth.middleware')

const router = Router()

router
    .post('/add',verifyToken, OrdersControllers.add)                                  /*Generar un nuevo pedido*/
    .get('/', verifyToken, OrdersControllers.getAll)                      /*El administrador puede visualizar todos los pedidos*/
    .get('/:id', verifyToken, OrdersControllers.getById)                              /*Los usuarios pueden ver sus pedidos con el Id.*/
    .put('/confirm/:id', verifyToken, OrdersControllers.confirm)                      /*El usuario confirma su pedido*/  
    .put('/edit/:id', verifyToken, verifyRole, OrdersControllers.updateRegistry)      /*El administrador puede cambiar el estado del pedido*/
    .delete('/delete/:id', verifyToken, verifyRole, OrdersControllers.delete)         /*El administrador puede eliminar un pedidd con el Id.*/

module.exports = router