const router = require ('express').Router();
const {verifyToken, verifyRole} = require('../middlewares/auth.middleware')
const PaymentMethodsControllers = require('../controllers/payment_methods.controllers');

//const router = Router({mergeParams:true});

router
    .post('/add', verifyToken, verifyRole, PaymentMethodsControllers.add)                     /*Create*/
    .get('/', verifyToken, PaymentMethodsControllers.getAll)
    .get('/:id', verifyToken, PaymentMethodsControllers.getById)                              /*Read*/
    .put('/edit/:id', verifyToken, verifyRole, PaymentMethodsControllers.updateRegistry)      /*Update*/
    .delete('/delete/:id', verifyToken, verifyRole, PaymentMethodsControllers.delete)         /*Delete*/


module.exports = router;