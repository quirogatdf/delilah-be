const { Router } = require ('express')
const LoginController = require('../controllers/login.controllers')
const { verifyNull } = require('../middlewares/auth.middleware')

const router = Router();

router.post('/login',verifyNull, LoginController.login)
router.post('/hello', LoginController.hello)


module.exports = router;
