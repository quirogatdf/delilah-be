const passport = require('passport');
const prepareGoogleStrategy = require('./google');
const prepareLinkedinStrategy = require('./linkedin');
function initialize() {
    prepareGoogleStrategy()
    prepareLinkedinStrategy()
}

module.exports = initialize;


