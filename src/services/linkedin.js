const passport = require('passport');
const LinkedinStrategy = require('passport-linkedin-oauth2').Strategy;

function prepareStrategy() {
  const strategy_name= 'linkedin';
  console.log(process.env.LINKEDIN_KEY);
  passport.use(strategy_name, new LinkedinStrategy({
    clientID: process.env.LINKEDIN_KEY,
    clientSecret: process.env.LINKEDIN_SECRET,
    callbackURL: process.env.LINKEDIN_CALLBACK,
    scope: ['r_emailaddress', 'r_liteprofile'],
  }, 
  function (accessToken, refreshToken, profile, done) {
    // asynchronous verification, for effect...
    return done(null, profile);
  }));
}

module.exports = prepareStrategy

